import React, { Component } from 'react';

import RaisedButton from 'material-ui/RaisedButton';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
import apple from './images/grid-list/apple.jpg';
import sky from './images/grid-list/sky.jpg';
import kounotori from './images/grid-list/kounotori.jpg';
import baby from './images/grid-list/baby.jpg';
import moon from './images/grid-list/moon.jpg';
import banana from './images/grid-list/banana.jpg';

import sammar from './images/grid-list/sammar.jpg';
import hige from './images/grid-list/hige.jpg';
import hair from './images/grid-list/hair.jpg';
import pi_man from './images/grid-list/pi-man.jpg';
import chusha from './images/grid-list/chusha.jpg';
import cold from './images/grid-list/cold.jpg';
import dream from './images/grid-list/dream.jpg';
import kushami from './images/grid-list/kushami.jpg';
import run from './images/grid-list/run.jpg';
import seki from './images/grid-list/seki.jpg';
import sleep from './images/grid-list/sleep.jpg';
import tabaco from './images/grid-list/tabaco.jpg';


import noImage from './images/grid-list/noImage.png';

import Dialog from 'material-ui/Dialog';
import {cyan500,indigo500,blue500,blue300,pink100,pink500,white,green500,lime500
} from 'material-ui/styles/colors';

import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';

import axios from 'axios'

class Timeline extends Component {

  constructor(props) {
      super(props);
      this.state = {
        open: false,
        selectedTalkLog:{
          Answer:"",
          Emotion:"",
          id:"",
          Question:"",
          Tag:"",
          Time:"",
          What:"",
        },
      }
  }

  handleOpen (talkLog) {
    this.setState({
      open: true,
      selectedTalkLog:talkLog
    });
    console.log(talkLog);
  }
  handleClose () {
    this.setState({open: false});
  }

  render() {
    let styles = {
      bot:{
        position: 'fixed',
        bottom: '0px',
        right: '0px',
        height: '600px'
      },
      root: {
        // display: 'flex',
        // flexWrap: 'wrap',
        // justifyContent: 'space-around',
      },
      gridList: {
        width: 'auto',
        overflowY: 'auto',
      },
    }

  let imgName = (what) => {
     if (what==='りんご' || what==='リンゴ') {
       return apple;
     }
     if (what==='そら') {
       return  sky;
     }
     if (what==='コウノトリ') {
       return  kounotori;
     }
     if (what==='赤ちゃん') {
       return  baby;
     }
     if (what==='つき') {
       return  moon;
     }
     if (what==='バナナ') {
       return  banana;
     }
     if (what==='なつ') {
       return  sammar;
     }
     if (what==='ひげ') {
       return  hige;
     }
     if (what==='かみ') {
       return  hair;
     }
     if (what==='ピーマン') {
       return  pi_man;
     }
     if (what==='夢') {
       return  dream;
     }
     if (what==='走る') {
       return  run;
     }
     if (what==='眠く') {
       return  sleep;
     }
     if (what==='寒い') {
       return  cold;
     }
     if (what==='くしゃみ') {
       return  kushami;
     }
     if (what==='咳') {
       return  seki;
     }
     if (what==='注射') {
       return  chusha;
     }
     if (what==='タバコ') {
       return  tabaco;
     }

     return noImage;
   }

   let emotion= (talkLog) => {
     if (typeof talkLog.Anger !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😡</span>)
     }
     if (typeof talkLog.Contempt !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😝</span>)
     }
     if (typeof talkLog.Disgust !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😱</span>)
     }
     if (typeof talkLog.Fear !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😨</span>)
     }
     if (typeof talkLog.Happiness !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😁</span>)
     }
     if (typeof talkLog.Neutral !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😑</span>)
     }
     if (typeof talkLog.Sadness !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😭</span>)
     }
     if (typeof talkLog.Surprise !=="undefined") {
       return (<span className="material-icons" style={{color:indigo500
         ,fontSize:50}}>😲</span>)
     }


      return false;
   }
   let emotionIconButton = (talkLog) => {
        return (<FlatButton style={{height:'auto'}} onTouchTap={this.handleOpen.bind(this,talkLog)}>
        {emotion(talkLog)}
        </FlatButton>)
   }



    let hoge = () => {
      console.log("hoge");
    }

    let actions = [
      <FlatButton
        label="閉じる"
        primary={true}
        style={{height:60}}
        onTouchTap={this.handleClose.bind(this)}
      />

    ];

    let changeAnswerMessge = (talkLog) => {
      if (talkLog.emotion< 0.6) {
        actions.push(<RaisedButton
          label="回答を変更する"
          primary={true}
          style={{height:60}}
          onTouchTap={this.handleClose.bind(this)}
        />);

        return (
          <div style={{color:pink500,fontSize:27}}>
          <br/>
          コグちゃんの返答に対する反応が良くありません。
          <br/>
          回答を変更しますか？
          </div>
        )
      }
      return false
    }




    return (
      <div className="App">
        <div className="App-header">
          <div style={styles.root}>
            <GridList
              cellHeight={180}
              style={styles.gridList}>

            <Subheader style={{fontSize:20}}>タカシ君とコグちゃんの会話</Subheader>
              {this.props.talkLogs.map((talkLog) => (
                <GridTile
                  cols={1}
                  key={talkLog.id}
                  title={talkLog.Question}
                  subtitle={talkLog.Time}
                  actionIcon={emotionIconButton(talkLog)}
                >
                  <img  src={imgName(talkLog.What)} />
                </GridTile>
              ))}
            </GridList>
          </div>

        </div>
        <Dialog
          title={this.state.selectedTalkLog.Question}
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose.bind(this)}
        >
          コグちゃんの返答：{this.state.selectedTalkLog.Answer}
          <br/>
          <br/>
          それに対するタカシ君の感情：{emotion(this.state.selectedTalkLog)}
          {changeAnswerMessge(this.state.selectedTalkLog)}


        </Dialog>
      </div>
    );
  }
}
Timeline.DefaultProps={
  talkLogs : []
}

Timeline.PropTypes={
  talkLogs : React.PropTypes.any
}


export default Timeline;
